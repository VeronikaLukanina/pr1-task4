import sys
import random

def binary_search(arr, target):
    left, right = 0, len(arr) - 1
    
    while left <= right:
        mid = (left + right) // 2
        if arr[mid] == target:
            return mid
        elif arr[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
            
    return -1

def main():
    # Генерируем массив случайных чисел и сортируем его
    arr = [random.randint(1, 1000) for _ in range(100)]
    arr.sort()
    
    # Выводим сгенерированный массив (для наглядности)
    print("Сгенерированный массив:", arr)
    
    # Получаем значение для поиска из аргументов командной строки
    if len(sys.argv) != 2:
        print("Использование: python program.py <искомое_значение>")
        sys.exit(1)
    
    target = int(sys.argv[1])
    
    # Выполняем бинарный поиск
    index = binary_search(arr, target)
    
    # Выводим результат
    if index != -1:
        print(f"Значение {target} найдено на позиции {index}.")
    else:
        print(f"Значение {target} не найдено в массиве.")

if __name__ == "__main__":
    main()
